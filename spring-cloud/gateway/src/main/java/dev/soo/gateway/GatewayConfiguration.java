package dev.soo.gateway;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@RequiredArgsConstructor
public class GatewayConfiguration {

    private final AppConfiguration conf;

    @Bean
    @LoadBalanced
    public WebClient.Builder loadBalancedWebClientBuilder() {
        return WebClient.builder();
    }

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("customer", r -> r.path("/customers/**")
                        .filters(gfc -> gfc.rewritePath("/customers", "/api/v1/customers"))
                        .uri("lb://customer"))
                .route("account", r -> r.path("/accounts/**")
                        .filters(gfc -> gfc.rewritePath("/accounts", "/api/v1/accounts"))
                        .uri("lb://account"))
                .route("eureka1", r -> r.path("/eureka")
                        .filters(gfc -> gfc.rewritePath("/eureka", "/"))
                        .uri("http://" + conf.getEurekaServer() + ":" + conf.getEurekaPort()))
                .route("eureka2", r -> r.path("/eureka/**")
                        .uri("http://" + conf.getEurekaServer() + ":" + conf.getEurekaPort()))
                .route("config", r -> r.path("/config/**")
                        .uri("http://" + conf.getConfigServer() + ":" + conf.getConfigPort()))
                .build();
    }
}
