package dev.soo.gateway;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

//@ActiveProfiles("test")
@Disabled
@SpringBootTest(webEnvironment = RANDOM_PORT)
class GatewayApplicationTests {

    @Test
    void contextLoads() {
    }

}
