package dev.soo.gateway;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.net.URI;


class GatewayConfigurationTest {

    @ParameterizedTest
    @ValueSource(strings = {
            "http://localhost:8080/not-eureka",
            "http://localhost:8080/eureka",
            "http://localhost:8080/eureka/",
            "http://localhost:8080/eureka/api",
            "http://localhost:8080/eureka/api/",
            "http://localhost:8080/eureka/api/enything"
    })
    void testRegexp(String uriSource) {
        URI uri = URI.create(uriSource);
        String path = uri.getPath();
        String rawPath = uri.getRawPath();

        System.out.println("uri = " + uri);
        System.out.println("path = " + path);
        System.out.println("rawPath = " + rawPath);

        String newPath = path.replaceAll("/eureka/(?<segment>.*)", "/${segment}");
        System.out.println("newPath = " + newPath);

        String newPath2 = path.replaceAll("/eureka(?<segment>.*)", "${segment}");
        System.out.println("newPath2 = " + newPath2);
    }

}