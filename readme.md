## Build:

```shell
gradle clean build --parallel
docker-compose build
```

## Run
```shell
docker-compose up -d
```

## Run with scale
```shell
docker-compose up -d --scale account=3 --scale customer=3
```
