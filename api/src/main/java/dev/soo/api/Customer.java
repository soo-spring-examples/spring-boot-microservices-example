package dev.soo.api;

public record Customer(Long id, String code, String name) {
}
