package dev.soo.api;

import java.math.BigDecimal;

public record Account(Long id, Long customerId, String accountNumber, BigDecimal amount) {
}
