package dev.soo.customerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;
//import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
//import org.springframework.context.annotation.Bean;
//import org.testcontainers.containers.OracleContainer;
//import org.testcontainers.containers.PostgreSQLContainer;

@TestConfiguration(proxyBeanMethods = false)
class TestCustomerServiceApplication {

	// @Bean
	// @ServiceConnection
	// OracleContainer oracleContainer() {
	// return new OracleContainer("gvenzl/oracle-xe:latest");
	// }

	// @Bean
	// @ServiceConnection
	// PostgreSQLContainer<?> postgresContainer() {
	// return new PostgreSQLContainer<>("postgres:latest");
	// }

	public static void main(String[] args) {
		SpringApplication.from(CustomerServiceApplication::main).with(TestCustomerServiceApplication.class).run(args);
	}

}
