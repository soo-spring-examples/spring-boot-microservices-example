package dev.soo.customerservice.env;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app-conf")
@RequiredArgsConstructor
public class AppConfigurationController {

    private final AppConfiguration appConfiguration;

    @GetMapping
    public AppConfiguration getAppConfiguration() {
        return appConfiguration.clone();
    }
}
