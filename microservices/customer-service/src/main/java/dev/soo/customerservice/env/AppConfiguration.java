package dev.soo.customerservice.env;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("app")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppConfiguration {

    private String configServer;
    private Long configPort;
    private String eurekaServer;
    private Long eurekaPort;
    private String postgresHost;
    private Long postgresPort;

    public AppConfiguration clone() {
        return new AppConfiguration(configServer, configPort, eurekaServer, eurekaPort, postgresHost, postgresPort);
    }
}
