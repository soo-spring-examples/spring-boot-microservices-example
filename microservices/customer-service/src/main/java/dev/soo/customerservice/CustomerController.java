package dev.soo.customerservice;

import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {

	private final CustomerService service;

	public CustomerController(CustomerService service) {
		this.service = service;
	}

	@GetMapping
	public Collection<Customer> findAll() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	public CustomerWithAccounts findById(@PathVariable Long id) {
		return service.findById(id);
	}

	@PostMapping
	public Customer create(@RequestBody Customer customer) {
		return service.create(customer);
	}

	@PutMapping("/{id}")
	public Customer update(@PathVariable Long id, @RequestBody Customer customer) {
		return service.update(id, customer);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		service.delete(id);
	}

}
