package dev.soo.customerservice;

import org.springframework.data.annotation.Id;

public record Customer(@Id Long id, String code, String name) {
}
