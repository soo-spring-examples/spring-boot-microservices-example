package dev.soo.customerservice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.ErrorResponseException;

import java.io.Serial;
import java.net.URI;

public class CustomerNotFoundException extends ErrorResponseException {

    @Serial
    private static final long serialVersionUID = 9148726055909377905L;

    private static final String TITLE = "Customer not found!";
    private static final String MESSAGE_TEMPLATE = "Customer id #%s not found!";

    public CustomerNotFoundException(Long id) {
        super(HttpStatus.NOT_FOUND, problemDetail(id), null);
    }

    private static ProblemDetail problemDetail(Long id) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, MESSAGE_TEMPLATE.formatted(id));
        problemDetail.setType(URI.create("/errors/not-found"));
        problemDetail.setTitle(TITLE);
        problemDetail.setProperty("customer.id", id);

        return problemDetail;
    }

}
