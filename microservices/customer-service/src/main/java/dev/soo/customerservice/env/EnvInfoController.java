package dev.soo.customerservice.env;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.TreeMap;

@RestController
@RequestMapping("/env-info")
public class EnvInfoController {

    private final Environment environment;

    private final int port;

    public EnvInfoController(Environment environment, @Value("${server.port}") int port) {
        this.environment = environment;
        this.port = port;
    }

    @GetMapping
    public Map<String, String> getInfo() throws UnknownHostException {
        return new TreeMap<>(Map.of(
                "port", String.valueOf(port),
                "server.port", String.valueOf(String.valueOf(environment.getProperty("server.port"))),
                "local.address", InetAddress.getLocalHost().getHostAddress(),
                "local.host.name", InetAddress.getLocalHost().getHostName(),
                "remote.address", InetAddress.getLoopbackAddress().getHostAddress(),
                "remote.host.name", InetAddress.getLoopbackAddress().getHostName()
        ));
    }

}
