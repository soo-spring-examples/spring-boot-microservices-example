package dev.soo.customerservice;

import dev.soo.api.Account;

import java.util.Collection;

public record CustomerWithAccounts(Long id, String code, String name, Collection<Account> accounts) {

    public static CustomerWithAccounts of(Customer customer, Collection<Account> accounts) {
        return new CustomerWithAccounts(customer.id(), customer.code(), customer.name(), accounts);
    }
}
