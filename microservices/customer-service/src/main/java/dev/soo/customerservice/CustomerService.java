package dev.soo.customerservice;

import dev.soo.api.Account;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CustomerService {

	private final CustomerRepository repository;
	private final AccountExternalService accountExternalService;

	public CustomerService(CustomerRepository repository, AccountExternalService accountExternalService) {
		this.repository = repository;
		this.accountExternalService = accountExternalService;
	}

	public Collection<Customer> findAll() {
		return (Collection<Customer>) repository.findAll();
	}

	public CustomerWithAccounts findById(Long id) {
		return repository.findById(id)
				.map(c -> {
					Collection<Account> accounts = accountExternalService.getAccountsForCustomer(c.id());
					return CustomerWithAccounts.of(c, accounts);
				})
				.orElseThrow(() -> new CustomerNotFoundException(id));
	}

	public Customer create(Customer customer) {
		return repository.save(customer);
	}

	public Customer update(Long id, Customer customer) {
		Customer validCustomer = new Customer(id, customer.code(), customer.name());
		return repository.save(validCustomer);
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}
}
