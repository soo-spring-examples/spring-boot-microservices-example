package dev.soo.customerservice;

import dev.soo.api.Account;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
public class AccountExternalService {

	private final RestTemplate restTemplate;

	public AccountExternalService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public Collection<Account> getAccountsForCustomer(Long customerId) {
		return List.of(Objects.requireNonNull(restTemplate.getForObject("http://account/api/v1/accounts/customer/" + customerId, Account[].class)));
	}
}
