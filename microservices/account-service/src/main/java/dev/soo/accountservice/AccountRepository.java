package dev.soo.accountservice;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

	Collection<Account> findAllByCustomerId(Long customerId);
}
