package dev.soo.accountservice;

import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/v1/accounts")
public class AccountController {

    private final AccountService service;

    public AccountController(AccountService service) {
        this.service = service;
    }

    @GetMapping
    public Collection<Account> findAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Account findOne(@PathVariable Long id) {
        return service.findById(id).orElseThrow(() -> new AccountNotFoundException(id));
    }

    @GetMapping("/customer/{customerId}")
    public Collection<Account> findAllByCustomerId(@PathVariable Long customerId) {
        return service.findAllByCustomerId(customerId);
    }

    @PostMapping
    public Account create(@RequestBody Account account) {
        return service.create(account);
    }

    @PutMapping("/{id}")
    public Account update(@PathVariable Long id, @RequestBody Account account) {
        return service.update(id, account);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
