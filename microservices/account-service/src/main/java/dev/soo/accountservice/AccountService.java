package dev.soo.accountservice;

import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class AccountService {

    private final AccountRepository repository;

    public AccountService(AccountRepository repository) {
        this.repository = repository;
    }

    public Collection<Account> findAll() {
        return (Collection<Account>) repository.findAll();
    }

    public Optional<Account> findById(Long id) {
        return repository.findById(id);
    }

    public Collection<Account> findAllByCustomerId(Long customerId) {
        return repository.findAllByCustomerId(customerId);
    }

    public Account create(Account account) {
        return repository.save(account);
    }

    public Account update(Long id, Account account) {
        Account account1 = new Account(id, account.customerId(), account.accountNumber(), account.amount());
        return repository.save(account1);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
