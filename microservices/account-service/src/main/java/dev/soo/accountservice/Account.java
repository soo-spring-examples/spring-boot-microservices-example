package dev.soo.accountservice;

import org.springframework.data.annotation.Id;

import java.math.BigDecimal;

public record Account(@Id Long id, Long customerId, String accountNumber, BigDecimal amount) {
}
