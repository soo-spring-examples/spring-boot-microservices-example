package dev.soo.accountservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = RANDOM_PORT)
class AccountServiceApplicationTests extends IntegrationTestBase {

    @Autowired
    private AccountService service;

    @Test
    void contextLoads() {
    }

    @Test
    void testFindAll() {
        Collection<Account> accounts = service.findAll();
        assertAll(
                () -> assertNotNull(accounts),
                () -> assertFalse(accounts.isEmpty()),
                () -> assertEquals(12, accounts.size())
        );
    }
}
